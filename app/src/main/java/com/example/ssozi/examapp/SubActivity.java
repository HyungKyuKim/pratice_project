package com.example.ssozi.examapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SubActivity extends AppCompatActivity {

    private List<items> item;
    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        rv=(RecyclerView)findViewById(R.id.recycler);

        rv.setLayoutManager(new GridLayoutManager(this, 3));

        rv.setHasFixedSize(true);

        List<items> list = new ArrayList<>();

        for(int i = 0; i<1000; i++) {
                list.add(new items("아이템" + i));
        }

        reAdap adapter = new reAdap(list);

        rv.setAdapter(adapter);

        Intent intent = getIntent();
        String msg = intent.getStringExtra("toss");

        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

        Toolbar toolbar2 = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar2);
        getSupportActionBar().setTitle("두번째 액티비티에요");

        toolbar2.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar2.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
