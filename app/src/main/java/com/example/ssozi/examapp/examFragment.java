package com.example.ssozi.examapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static android.R.attr.name;
import static android.R.attr.text;

/**
 * Created by SSozi on 2016. 11. 6..
 */

public class examFragment extends android.support.v4.app.Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        int resId = R.layout.fragment_1;
        View view = inflater.inflate(R.layout.fragment_1, container, false);
        final EditText fragEdit =(EditText)view.findViewById(R.id.editText);
        final TextView fragText = (TextView)view.findViewById(R.id.textView);
        final Button fragBtn = (Button) view.findViewById(R.id.button);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable edit) {
//                   fragText.setText(edit);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                fragText.setText(s.toString());
            }
        };

        fragEdit.addTextChangedListener(textWatcher);

        fragBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),SubActivity.class);
                intent.putExtra("toss", fragText.getText().toString());
                startActivity(intent);
                //getActivity().finish();
            }
        });

        return view;

    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }
}
