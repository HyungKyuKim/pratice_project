package com.example.ssozi.examapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import static com.example.ssozi.examapp.R.id.imageView;

/**
 * Created by SSozi on 2016. 11. 7..
 */

public class examFragment2 extends android.support.v4.app.Fragment{
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        int resId = R.layout.fragment_2;
        View view = inflater.inflate(R.layout.fragment_2, container, false);

//        Glide.with(this).load("http://i.imgur.com/DvpvklR.png").into(imageV);
        ImageView imageV = (ImageView) view.findViewById(R.id.imageView);

        Glide.with(this)
                .load("http://i.imgur.com/DvpvklR.png")
                .fitCenter()
                .into(imageV);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }
}
