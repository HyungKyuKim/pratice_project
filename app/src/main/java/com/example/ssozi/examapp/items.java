package com.example.ssozi.examapp;

/**
 * Created by SSozi on 2016. 11. 7..
 */

public class items {
    String title;

    public items(String title) {
        this.title = title;
    }

    public String getItems() {
        return title;
    }

    public void setItems(String title) {
        this.title = title;
    }
}
