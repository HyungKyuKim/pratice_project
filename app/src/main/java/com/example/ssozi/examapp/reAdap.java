package com.example.ssozi.examapp;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by SSozi on 2016. 11. 7..
 */

public class reAdap extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<items> list;
        reAdap(List<items> list){
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType){
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
                viewHolder = new ItemViewHolder(view);
                break;
            case 2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item2, parent, false);
                viewHolder = new Item2ViewHolder(view);
                break;
            default:
                break;
        }

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ItemViewHolder){
            ((ItemViewHolder)holder).text.setText(list.get(position).title);
        }
        else if(holder instanceof Item2ViewHolder){
            ((Item2ViewHolder)holder).text2.setText(list.get(position).title);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;

        switch (position%2){
            case 0:
                viewType = 1;
                break;
            case 1:
                viewType = 2;
                break;
        }

        return viewType;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView text;

        public ItemViewHolder(View itemView) {
            super(itemView);
            text = (TextView)itemView.findViewById(R.id.title);
        }
    }

    class Item2ViewHolder extends RecyclerView.ViewHolder {
        TextView text2;

        public Item2ViewHolder(View itemView) {
            super(itemView);
            text2 = (TextView)itemView.findViewById(R.id.title2);

        }
    }
}